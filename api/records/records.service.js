const records = require("../../models/records");
const recordsModel = records.recordsModel;
const logger = require('../../logger');


// Records Service Layer to fetch Records based on filter Body
const getRecordsWithCount = async function(req, res) {
  let categoryDataRecords = null;

  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const minCount = req.body.minCount;
  const maxCount = req.body.maxCount;


try {
    	categoryDataRecords = await recordsModel.aggregate([
	             {
    	        		$match: {
    	        			createdAt: {$gte: new Date(startDate), $lte: new Date(endDate)},
    	        		},
	             },
               {
                    $project: {
                      _id: 0,
                      key: 1,
                      createdAt: 1,
                      totalCount: {$sum: "$counts"},
                    },
               },
               {
                	$match: {
                		totalCount: {
                            $gte: minCount,
                            $lte: maxCount,
                        },
                	},
                },
                { 
                    $skip: Number(req.query.limit)*((req.query.page)-1) || 0
                },
    			      {
                    $limit: Number(req.query.limit)||10
                },
            ]);

        return categoryDataRecords;

} catch (err) {
    logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

    return categoryDataRecords;
  }
};

module.exports = {
  getRecordsWithCount,
}
