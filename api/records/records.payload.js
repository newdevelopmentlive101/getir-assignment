const resources = require("../../utils/response.payload");
const sendResponse = resources.sendResponse;

const platformcodes = require("../../utils/constants");
const validators = require("../../utils/validators");


const getRecordsWithCountResponse = function(categoryData) {
  const data = {records: null};

  let responseDict = {};

  if (categoryData) {
    data.records = categoryData;
    responseDict = sendResponse(platformcodes.code.Success, platformcodes.statusMessages.Success, data);
  } else {
    responseDict = sendResponse(platformcodes.code.RecordsUnavailable, platformcodes.statusMessages.RecordsUnavailable, data);
  }

  return responseDict;
};


// Validation Middleware for Request List Route 
// If validation succeeds it moves to next in request chain
const validateRecordRequest = function(req, res, next) {
  const data = {records: null};
  let responseDict = {};

  const startDate = req.body.startDate || null;
  const endDate = req.body.endDate || null;

  const minCount = req.body.minCount || null;
  const maxCount = req.body.maxCount || null;

  // isCorrectRecordsDateFilter will validate dates to be in correct format : "YYYY-MM-DD"
  if (!validators.isCorrectRecordsDateFilter(startDate) || !validators.isCorrectRecordsDateFilter(endDate) 
    || (typeof minCount != "number") || (typeof maxCount != "number")) {

    responseDict = sendResponse(platformcodes.code.UnprocessableEntity, platformcodes.statusMessages.UnprocessableEntity, data);
    return res.status(406).send(responseDict);
  }

  next();
};


module.exports = {
  getRecordsWithCountResponse,
  validateRecordRequest,
}
