const router = require("express").Router();
const recordController = require("./records.controller");
const recordValidator = require("./records.payload");


router.post("/", recordValidator.validateRecordRequest, recordController.getlist);

exports = module.exports = router;
