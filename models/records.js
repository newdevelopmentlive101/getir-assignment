const mongoose = require("mongoose");

const recordSchema = mongoose.Schema({
  key: {type: String},
  createdAt: {type: Date, default: new Date()},
  counts: [{type: Number}],
  value: {type: String},
});

recordSchema.index({createdAt: -1});

const recordsModel = mongoose.model('records', recordSchema);

module.exports = {
  recordsModel,
};
