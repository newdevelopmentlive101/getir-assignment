const sendResponse = function(status, result, message) {
  const dict = message;

  dict.code = status;
  dict.msg = result;

  return dict;
};


module.exports = {
  sendResponse,
}
