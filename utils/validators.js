const moment = require('moment');

// Validates if date string is in correct format : "YYYY-MM-DD" 
const isCorrectRecordsDateFilter = function(date) {
	let isValid = false;

	if(!date){
		return isValid;
	}

	if (typeof date != "string") { return isValid; }

	const formats = ['YYYY-MM-DD'];
    isValid = moment(date, formats, true).isValid();

    return isValid;

};

module.exports = {
  isCorrectRecordsDateFilter,
}
