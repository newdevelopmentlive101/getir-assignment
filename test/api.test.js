const chai = require('chai');
const supertest = require("supertest");
const app = require('../server');
const mongoose = require("mongoose");


beforeAll(async() => {
  jest.setTimeout(80000);
  try {
  	await mongoose.connect("mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true", 
  		{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
	}catch (err) {
	    console.log('error: ' + err)
	  }
});


test("POST /api/records", async () => {

  await supertest(app).post("/api/records?page=1&limit=2")
  	.send({ 
			"startDate": "2014-01-26", 
			"endDate": "2016-05-29", 
			"minCount": 300, 
			"maxCount": 5000 
	})
    .expect(200)
    .then((response) => {
      // Check data
      expect(Array.isArray(response.body.records)).toBeTruthy();
      expect(response.body.records.length).toBeGreaterThan(0);
      expect(response.body.code).toBe(0);
      expect(response.body.msg).toBe("Success");
    });
});

