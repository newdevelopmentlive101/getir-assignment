#!/usr/bin/env node

const express = require("express");
const routes = require("./routes");
const cors = require('cors');
const logger = require('./logger');
const morgan = require("morgan");
const swaggerUI = require("swagger-ui-express");
const docs = require('./docs');


if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: `${__dirname}/config/.env`,
  })
} else if (process.env.NODE_ENV === 'production') {
  require('dotenv').config({
    path: `${__dirname}/config/.env.production`,
  })
}

console.log(`${process.env.NODE_ENV} server...`);
console.log(process.env.MONGO_URI);


const app = express();

app.use(express.json());
app.use(cors());
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(docs));


if (process.env.NODE_ENV === 'development') {
  app.use(morgan(function(tokens, req, res) {
    return JSON.stringify({
      'remote-address': tokens['remote-addr'](req, res),
      'time': tokens['date'](req, res, 'iso'),
      'method': tokens['method'](req, res),
      'url': tokens['url'](req, res),
      'http-version': tokens['http-version'](req, res),
      'status-code': tokens['status'](req, res),
      'content-length': tokens['res'](req, res, 'content-length'),
      'referrer': tokens['referrer'](req, res),
      'user-agent': tokens['user-agent'](req, res),
    });
  }, {stream: logger.stream}));
}


app.use("/api", routes.router);


app.get("/status", (req, res) => {
  const dict = {
    status: "Service up!!",
    result: "YES",
  };
  return res.status(200).send(dict);
});


module.exports = app;
