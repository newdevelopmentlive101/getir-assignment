const app = require("./server");
const db = require("./config/db");


db.connectDb().catch(error => console.error(error))

app.listen(process.env.PORT, () => {
	console.log(`Server started at ${process.env.PORT}`);
});