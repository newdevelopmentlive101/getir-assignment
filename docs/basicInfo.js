module.exports = {
  openapi: "3.0.3", // present supported openapi version
  info: {
    title: "Getir Assignment API", // short title.
    description: "A simple Test API", //  desc.
    version: "1.0.0", // version number
    contact: {
      name: "Abhishek Dutt", // your name
      email: "abhishekdutt.iitr@gmail.com", // your email
      url: "", // your website
    },
  },
};
